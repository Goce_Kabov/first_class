import React from "react";

const ReverseButton = ({ handleClick, buttonText }) => {
  // const handleClick = () => alert("clicked");
  return <button onClick={handleClick}>{buttonText}</button>;
};

export default ReverseButton;
