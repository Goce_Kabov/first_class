import React, { Component } from "react";
import "./App.css";
import HeaderTitle from "./HeaderTitle";
import ReverseButton from "./ReverseButton";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "ASD",
      title: this.props.title,
      buttonText: "Click me",
      isClicked: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => {
      return {
        title: this.state.title.split("").reverse().join(""),
        isClicked: !prevState.isClicked,
        buttonText: !prevState.isClicked ? "Unclick me" : "Click me",
      };
    });
  }

  render() {
    return (
      <div className="App">
        <HeaderTitle title={this.state.title} />
        <ReverseButton
          buttonText={this.state.buttonText}
          handleClick={this.handleClick}
        />
      </div>
    );
  }
}

export default App;
